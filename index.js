import BingMap from './src/components/BingMap'
import BingMapInfoBox from './src/components/BingMapInfoBox'
import BingMapLayer from './src/components/BingMapLayer'
import BingMapPushPin from './src/components/BingMapPushPin'
import ComponentBase from './src/components/ComponentBase'
import BingConversions from './src/services/BingConversions'

const Components = {
  BingMap,
  BingMapInfoBox,
  BingMapLayer,
  BingMapPushPin,
  ComponentBase,
}
export default {
  mixins: {
    componentBase: ComponentBase,
  },
  components: {
    map: BingMap,
    layer: BingMapLayer,
    pushpin: BingMapPushPin,
    infobox: BingMapInfoBox,
  },
  services: {
    conversions: BingConversions,
  },
  install: function (Vue, options) {
    Object.keys(Components).forEach(name => {
      Vue.component(name, Components[name])
    })
  }
}
