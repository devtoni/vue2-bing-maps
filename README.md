# vue2-bing-maps

Credits to https://github.com/ksullivan1856/vue-bing-maps as this is mainly the same code but with some modifications for our needs.

Bing V8 maps implementation for vue

## Table of contents

- [Installation](#installation)
- [Usage](#usage)

# Installation

```
npm install --save vue2-bing-maps
```

## Default import

Install all the components:

```javascript
import Vue from 'vue'
import Vue2BingMaps from 'vue2-bing-maps'

Vue.use(Vue2BingMaps)
```

**⚠️ You need to configure your bundler to compile `.vue` files.** More info [in the official documentation](https://vuejs.org/v2/guide/single-file-components.html).

# Usage

```
<bing-map :credentials="mapCredentials" :options="mapOptions" v-if="mapVisible">
    <bing-map-layer name="activeFlightsLayer" 
                    @layer-click="handleEvent" 
                    :visible="pinsVisible">
        <bing-map-pushpin v-for="item in pins" :metadata="item.metadata" :location="item.location" :options="item.options"></bing-map-pushpin>
        <bing-map-infobox :options="tooltip">
            <div class="customInfobox">{{tooltip.description}}</div>
        </bing-map-infobox>
    </bing-map-layer>
</bing-map>
```

- mapCredentials: String Api key 
- mapOptions (the map options available [here](https://docs.microsoft.com/en-us/bingmaps/v8-web-control/map-control-api/mapoptions-object))
- Each component accepts the same kind of events that in V8 Maps, for example for [pushpin](https://docs.microsoft.com/en-us/bingmaps/v8-web-control/map-control-api/pushpin-class).

The structure to specify an event is the following:

  - @itemTypeName-eventName
  - Each component has defined a itemType name. For example the layer itemType is **layer**
  - To define a onclick event for the layer you just need to define like this @layer-click

# Plugin Development

## Use it in another project

While developping, you can follow the install instructions of your plugin and link it into the project that uses it.

In the plugin folder:

```
npm link
```

In the other project folder:

```
npm link vue2-bing-maps
```

This will install it in the dependencies as a symlink, so that it gets any modifications made to the plugin.

## Publish to npm

You may have to login to npm before, with `npm adduser`. The plugin will be built in production mode before getting published on npm.

```
npm publish
```

