export default {
  loadedModules: [],
  deepClone: function(item) {
    return JSON.parse(JSON.stringify(item))
  }
}
